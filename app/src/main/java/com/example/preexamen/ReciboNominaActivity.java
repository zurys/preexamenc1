package com.example.preexamen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.Random;

public class ReciboNominaActivity extends AppCompatActivity {

    private EditText txtHorasTrabajadas;
    private EditText txtHorasExtras;
    private RadioGroup radioGroupPuesto;
    private TextView txtPagoSubtotal;
    private TextView txtPagoImpuesto;
    private TextView txtTotalPago;
    private TextView txtNumeroRecibo;
    private TextView txtNombreUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        // Bind views
        txtHorasTrabajadas = findViewById(R.id.txtHorasTrabajadas);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        radioGroupPuesto = findViewById(R.id.radioGroupPuesto);
        txtPagoSubtotal = findViewById(R.id.txtPagoSubtotal);
        txtPagoImpuesto = findViewById(R.id.txtPagoImpuesto);
        txtTotalPago = findViewById(R.id.txtTotalPago);
        txtNumeroRecibo = findViewById(R.id.txtNumeroRecibo);
        txtNombreUsuario = findViewById(R.id.txtNombreUsuario);

        // Generate a random receipt number and set it to the TextView
        int numeroRecibo = new Random().nextInt(10000); // Example range 0-9999
        txtNumeroRecibo.setText("Número de Recibo: " + numeroRecibo);

        // Get the user name from the Intent and set it to the TextView
        String nombreUsuario = getIntent().getStringExtra("Cliente");
        txtNombreUsuario.setText("Nombre del Trabajador: " + nombreUsuario);

        Button btnCalcular = findViewById(R.id.BtnCalcular);
        Button btnLimpiar = findViewById(R.id.BtnLimpiar);
        Button btnCerrar = findViewById(R.id.BtnCerrar);

        // Set onClickListeners
        btnCalcular.setOnClickListener(this::calcular);
        btnLimpiar.setOnClickListener(this::limpiar);
        btnCerrar.setOnClickListener(this::cerrar);
    }

    private void calcular(View view) {
        try {
            int horasNormales = Integer.parseInt(txtHorasTrabajadas.getText().toString());
            int horasExtras = Integer.parseInt(txtHorasExtras.getText().toString());
            int puesto = getPuestoSeleccionado();

            double porcentajeImpuesto = 16.0;

            String nombreUsuario = txtNombreUsuario.getText().toString().replace("Nombre del Trabajador: ", "");
            ReciboNomina recibo = new ReciboNomina(1, nombreUsuario, horasNormales, horasExtras, puesto, porcentajeImpuesto);

            double subtotal = recibo.calcularSubtotal();
            double impuesto = recibo.calcularImpuesto();
            double totalAPagar = recibo.calcularTotalAPagar();

            txtPagoSubtotal.setText("Subtotal: $" + subtotal);
            txtPagoImpuesto.setText("Impuesto: $" + impuesto);
            txtTotalPago.setText("Total a pagar: $" + totalAPagar);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void limpiar(View view) {
        txtHorasTrabajadas.setText("");
        txtHorasExtras.setText("");
        radioGroupPuesto.check(R.id.auxiliar);
        txtPagoSubtotal.setText("Subtotal: $");
        txtPagoImpuesto.setText("Impuesto: $");
        txtTotalPago.setText("Total a pagar: $");

        // Generate a new random receipt number
        int numeroRecibo = new Random().nextInt(10000);
        txtNumeroRecibo.setText("Número de Recibo: " + numeroRecibo);
    }

    private void cerrar(View view) {
        finish();
    }

    private int getPuestoSeleccionado() {
        int selectedId = radioGroupPuesto.getCheckedRadioButtonId();
        if (selectedId == R.id.auxiliar) {
            return 1;
        } else if (selectedId == R.id.albañil) {
            return 2;
        } else if (selectedId == R.id.obra) {
            return 3;
        } else {
            throw new IllegalArgumentException("Puesto inválido");
        }
    }
}
