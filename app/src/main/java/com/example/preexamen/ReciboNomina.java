package com.example.preexamen;

public class ReciboNomina {
    // Attributes
    private int numRecibo;
    private String nombre;
    private float horasNormales;
    private float horasExtras;
    private int puesto;

    private double porcentajeImpuesto;

    // Constructor
    public ReciboNomina(int numRecibo, String nombre, float horasNormales, float horasExtras, int puesto, double porcentajeImpuesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasNormales = horasNormales;
        this.horasExtras = horasExtras;
        this.puesto = puesto;
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    //Constructor Empty
    public ReciboNomina() {
        this.numRecibo = 0;
        this.nombre = "";
        this.horasNormales = 0.0f;
        this.horasExtras = 0.0f;
        this.puesto = 0;
        this.porcentajeImpuesto = 0.0f;
    }

    //Constructor Empty
    public ReciboNomina(ReciboNomina nomina) {
        this.numRecibo = nomina.numRecibo;
        this.nombre = nomina.nombre;
        this.horasNormales = nomina.horasNormales;
        this.horasExtras = nomina.horasExtras;
        this.puesto = nomina.puesto;
        this.porcentajeImpuesto = nomina.porcentajeImpuesto;
    }

    // Getters and Setters
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasNormales;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasNormales = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public double getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(double porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    //Functions
    public double calcularPagoBase() {
        double pagoBase = 200.0;
        switch (puesto) {
            case 1:
                return pagoBase + pagoBase * 0.20;
            case 2:
                return pagoBase + pagoBase * 0.50;
            case 3:
                return pagoBase + pagoBase * 1.00;
            default:
                throw new IllegalArgumentException("Puesto inválido");
        }
    }

    public double calcularSubtotal() {
        double pagoBase = calcularPagoBase();
        return (horasNormales * pagoBase) + (horasExtras * pagoBase * 2);
    }

    public double calcularImpuesto() {
        double subtotal = calcularSubtotal();
        return subtotal * (porcentajeImpuesto / 100);
    }

    public double calcularTotalAPagar() {
        double subtotal = calcularSubtotal();
        double impuesto = calcularImpuesto();
        return subtotal - impuesto;
    }

    @Override
    public String toString() {
        return "ReciboNomina{" +
                "numeroRecibo=" + numRecibo +
                ", nombre='" + nombre + '\'' +
                ", horasNormales=" + horasNormales +
                ", horasExtras=" + horasExtras +
                ", puesto=" + puesto +
                ", porcentajeImpuesto=" + porcentajeImpuesto +
                ", subtotal=" + calcularSubtotal() +
                ", impuesto=" + calcularImpuesto() +
                ", totalAPagar=" + calcularTotalAPagar() +
                '}';
    }


}

